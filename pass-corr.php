<?php
$lcLetters = ['a','b','c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
$ucLetters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
$numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
$specials =  ['&', '~', '#', '%', '$', '*', '@', '/'];


$length = isset($_GET['length'])?intval($_GET['length']):0;
$hasSpecials = isset($_GET['specials']);
$hasUC = isset($_GET['upper']);
$hasNumber = isset($_GET['numbers']);

$totalLetters = [];
$totalLetters = array_merge($totalLetters, $lcLetters);

if($hasUC){
    $totalLetters = array_merge($totalLetters, $ucLetters); // ajoute les maj
}

if($hasNumber){
    $totalLetters = array_merge($totalLetters, $numbers); // ajoute les nombres
}

if($hasSpecials){
    $totalLetters = array_merge($totalLetters, $specials); // ajoute les specs
}



function generatePasswd($letters,$length){
    $password = "";
    for($i = 0; $i < $length; $i++){
        $password.= randomLetter($letters);
    }
    return $password;
} // génère un mot de passe avec les caractères de $letters et de longueur $length

function randomLetter($letters){
    return $letters[rand(0,sizeof($letters)-1)];
} // retourne une lettre aléatoire du tableau $letters

// echo randomLetter($numbers);
echo generatePasswd($totalLetters, $length);

?>
<form action="" method="get">
        <div>
        <label for="length">longueur du mdp</label><br>
            <input id="length" type="number" name="length" min="1" max="100" value="<?php echo $length ?>">
        </div>
        <div>
        <label><input type="checkbox" name="upper" <?php if($hasUC) echo 'checked' ?>>Majuscules</label>
        </div>
        <div>
            <label><input type="checkbox" name="numbers" <?php if($hasNumber) echo 'checked' ?>Nombres</label>
        </div>
        <div>
            <label><input type="checkbox" name="specials" <?php if($hasSpecials) echo 'checked' ?>Caractères spéciaux</label>
        </div>
        <input type="submit">
    </form>