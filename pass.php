<?php
$lowerCaseLetters = "azertyuiopqsdfghjklmwxcvbn";
$upperCaseLetters = strtoupper($lowerCaseLetters);
$digits = "0123456789";
$specials = '&~#^$-_/!@%*';

function randomLetter($string)
{
    $length = strlen($string);
    $randomNumber = rand(0, $length - 1);
    return $string[$randomNumber];
}

function generatePassword($letters, $length)
{
    $password = '';
    for ($i = 0; $i < $length; $i++) {
        $password .= randomLetter($letters);
    }
    return $password;
}

if (isset($_GET['length']) && $_GET['length']>0) {
    
        $letters = $lowerCaseLetters;
        if (isset($_GET['upper'])) {
            $letters .= $upperCaseLetters;
        }
        if (isset($_GET['digits'])) {
            $letters .= $digits;
        }
        if (isset($_GET['specials'])) {
            $letters .= $specials;
        }
        $passwd = generatePassword($letters, intval($_GET['length']));
    ?>
    <hr>
    <h1>Votre mot de passe</h1>
    <p><?= $passwd ?></p>
    <hr>
    <?php
}
function checked($name) {
    if (isset($_GET[$name])){
        return 'checked';
    } else {
        return '';
    }
}
?>
    <form action="" method="get">
        <div>
            <input type="number" name="length" min="1" max="100" value="<?= isset($_GET['length'])?$_GET['length']:0 ?>">
        </div>
        <div>
            <label><input type="checkbox" name="upper" <?= checked('upper') ?>>Majuscules</label>
        </div>
        <div>
            <label><input type="checkbox" name="digits" <?= checked('digits') ?>>Nombres</label>
        </div>
        <div>
            <label><input type="checkbox" name="specials" <?= checked('specials') ?>>Caractères spéciaux</label>
        </div>
        <input type="submit">
    </form>
<?php
